#include <stdio.h>

#define max 10

int a[11] = { 15, 10, 25, 32, 23, 88, 53, 67, 31, 90, 0 };
int b[10];

void mergsort(int primeiro, int meio, int ultimo) {
   int l1, l2, i;

   for(l1 = primeiro, l2 = meio + 1, i = primeiro; l1 <= meio && l2 <= ultimo; i++) {
      if(a[l1] <= a[l2])
         b[i] = a[l1++];
      else
         b[i] = a[l2++];
   }

   while(l1 <= meio)
      b[i++] = a[l1++];

   while(l2 <= ultimo)
      b[i++] = a[l2++];

   for(i = primeiro; i <= ultimo; i++)
      a[i] = b[i];
}

void sort(int primeiro, int ultimo) {
   int meio;

   if(primeiro < ultimo) {
      meio = (primeiro + ultimo) / 2;
      sort(primeiro, meio);
      sort(meio+1, ultimo);
      mergsort(primeiro, meio, ultimo);
   } else {
      return;
   }
}

int main() {
   int i;

   printf("Lista antes\n");

   for(i = 0; i <= max; i++)
      printf("%d ", a[i]);

   sort(0, max);

   printf("\nLista depois\n");

   for(i = 0; i <= max; i++)
      printf("%d ", a[i]);
}
